package com.example.demo.service;

import com.example.demo.dto.IncorrectFieldsDto;

public interface SudokuService {

    boolean isValid();

    IncorrectFieldsDto getIncorrectSudokuDto();
}
