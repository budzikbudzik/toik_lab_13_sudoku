package com.example.demo.service;

import com.example.demo.dto.IncorrectFieldsDto;
import com.example.demo.dto.SudokuDto;
import com.example.demo.streams.Streams;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {
    final Streams myStream = new Streams();
    SudokuDto board = myStream.readData();
    private IncorrectFieldsDto incorrectSudokuDto;

    @Override
    public IncorrectFieldsDto getIncorrectSudokuDto() {
        return incorrectSudokuDto;
    }

    @Override
    public boolean isValid() {
        List<Integer> rowIds = new ArrayList<>();
        List<Integer> columnIds = new ArrayList<>();
        List<Integer> squareIds = new ArrayList<>();
        List<Integer> tempColumn = new ArrayList<>();

        // checking rows
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int frequencyRows = Collections.frequency(Arrays.asList(board.getSudoku()[i]), board.getSudoku()[i][j]);
                if (frequencyRows > 1) {
                    rowIds.add(i + 1);
                    break;
                }
            }
        }

        //checking columns
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                tempColumn.add(board.getSudoku()[j][i]);
            }
            for (Integer x : tempColumn) {
                if (Collections.frequency(tempColumn, x) > 1) {
                    columnIds.add(i + 1);
                    break;
                }
            }
            tempColumn = new ArrayList<>();

        }

        for (int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3) {
                for (int pos = 0; pos < 8; pos++) {
                    for (int pos2 = pos + 1; pos2 < 9; pos2++) {
                        if (board.getSudoku()[row + pos % 3][col + pos / 3].equals(board.getSudoku()[row + pos2 % 3][col + pos2 / 3])) {
                            switch (row) {
                                case 0:
                                    switch (col) {
                                        case 0:
                                            squareIds.add(1);
                                            break;
                                        case 3:
                                            squareIds.add(2);
                                            break;
                                        case 6:
                                            squareIds.add(3);
                                            break;
                                    }
                                    break;
                                case 3:
                                    switch (col) {
                                        case 0:
                                            squareIds.add(4);
                                            break;
                                        case 3:
                                            squareIds.add(5);
                                            break;
                                        case 6:
                                            squareIds.add(6);
                                            break;
                                    }
                                    break;
                                case 6:
                                    switch (col) {
                                        case 0:
                                            squareIds.add(7);
                                            break;
                                        case 3:
                                            squareIds.add(8);
                                            break;
                                        case 6:
                                            squareIds.add(9);
                                            break;
                                    }

                                    break;
                            }
                        }
                    }
                }
            }
        }

        if (!rowIds.isEmpty() || !squareIds.isEmpty() || !columnIds.isEmpty()) {
            incorrectSudokuDto = new IncorrectFieldsDto(rowIds, columnIds, squareIds);
            return false;
        }
        return true;
    }
}
