package com.example.demo.dto;

public class SudokuDto {

    Integer sudoku[][];

    public Integer[][] getSudoku() {
        return sudoku;
    }

    public SudokuDto(Integer[][] sudoku) {
        this.sudoku = sudoku;
    }

    public SudokuDto() {
    }
}
