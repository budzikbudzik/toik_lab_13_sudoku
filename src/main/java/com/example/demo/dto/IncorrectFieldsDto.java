package com.example.demo.dto;

import java.util.List;

public class IncorrectFieldsDto {
    private List<Integer> lineIds;
    private List<Integer> columnIds;
    private List<Integer> areaIds;

    public IncorrectFieldsDto() {
    }

    public IncorrectFieldsDto(List<Integer> lineIds, List<Integer> columnIds, List<Integer> areaIds) {
        this.lineIds = lineIds;
        this.columnIds = columnIds;
        this.areaIds = areaIds;
    }

    public List<Integer> getLineIds() {
        return lineIds;
    }

    public List<Integer> getColumnIds() {
        return columnIds;
    }

    public List<Integer> getAreaIds() {
        return areaIds;
    }
}
