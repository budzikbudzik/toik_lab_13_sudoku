package com.example.demo.streams;

import com.example.demo.dto.SudokuDto;

import java.io.*;

public class Streams {

    private static Integer[][] sudokuArray = new Integer[9][9];
    private static final String fileName = "sudoku.csv";
    private static String SEPERATOR = "/n";
    private static final String SPLIT_CHAR = ",";

    public static SudokuDto readData() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileName));
            int y = 0;
            while ((SEPERATOR = br.readLine()) != null) {
                String[] tab = SEPERATOR.split(SPLIT_CHAR);//tablica pozioma
                for (int x = 0; x < sudokuArray.length; x++) {
                    sudokuArray[y][x] = Integer.parseInt(tab[x]);
                }
                y++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Plik jest pusty");
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new SudokuDto(sudokuArray);
    }
}
