package com.example.demo.rest;

import com.example.demo.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api/sudoku")
public class SudokuApiController {

    @Autowired
    SudokuService sudokuService;

    @PostMapping(value = "/verify")
    public ResponseEntity verify() {
        if (sudokuService.isValid()) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body(sudokuService.getIncorrectSudokuDto());
    }
}
